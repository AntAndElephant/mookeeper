package org.ant.mookeeper.common.entity.io;

import java.io.BufferedReader;

import org.apache.commons.io.IOUtils;

import ch.ethz.ssh2.Connection;
import ch.ethz.ssh2.Session;

public class SSHResource {
	public Connection conn = null;
	public Session session = null;
	public BufferedReader reader = null;

	/** 关闭SSH操作所有占用的资源 */
	public void closeAllResource() {
		IOUtils.closeQuietly(reader);
		if ( null != session )
			session.close();
		if ( null != conn )
			conn.close();
	}

	public void setConn( Connection conn ) {
		this.conn = conn;
	}

	public void setSession( Session session ) {
		this.session = session;
	}

	public void setReader( BufferedReader reader ) {
		this.reader = reader;
	}
}
