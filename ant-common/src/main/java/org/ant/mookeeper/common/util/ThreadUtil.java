package org.ant.mookeeper.common.util;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Timer;
import java.util.TimerTask;

import org.ant.mookeeper.common.exception.IllegalParamException;

/**
 * 
 * Description: 线程类工具类
 * @author mxsm
 * @Date 2017-06-29
 */
public class ThreadUtil {

	/**
	 * 重复开启 threadNum 个线程来执行 runnable
	 * @param runnable 可执行任务
	 * @param threadNum 重复开启的线程个数
	 * @param sleepTime 启动完所有线程后，休息 ms
	 */
	public static void startThread( Runnable runnable, int threadNum, long sleepTime ) {

		for ( int i = 0; i < threadNum; i++ ) {
			Thread thread = new Thread( runnable, "Thread#" + i );
			thread.start();
		}
		try {
			Thread.sleep( sleepTime );
		} catch ( InterruptedException e ) {
		}
	}

	/**
	 * 开启 1 个线程来执行 runnable
	 * @param runnable 可执行任务
	 */
	public static void startThread( Runnable runnable ) {
		startThread( runnable, 1, 0 );
	}

	/**
	 * 重复开启 threadNum 个线程来执行 runnable
	 * @param runnable 可执行任务
	 * @param threadNum 重复开启的线程个数
	 */
	public static void startThread( Runnable runnable, long sleepTime ) {
		startThread( runnable, 1, sleepTime );
	}

	/**
	 * 定时执行一个任务
	 * @param task 实现TimerTask接口的任务实例
	 * @param delay 调用方法后要延时的毫秒数
	 * @param period 执行间隔
	 * @throws IllegalParamException 
	 */
	public static void scheduleAtFixedRateDelayTimeMillisDelay( TimerTask task, long delay, long period ) throws IllegalParamException {
		if( null == task )
			throw new IllegalParamException( "task 为空" );
		new Timer().scheduleAtFixedRate( task, delay, period );
	}
	
	/**
	 * 定时执行一个任务
	 * @param task 实现TimerTask接口的任务实例
	 * @param hourOfTomorrow 第二天的几点开始
	 * @param period 执行间隔
	 * @throws Exception 
	 * @throws IllegalParamException 
	 */
	public static void scheduleAtFixedRateDelayDaysHour( TimerTask task, int delayDays, int hourOfTomorrow, long period ) throws IllegalParamException, Exception {
		ThreadUtil.scheduleAtFixedRateDelayTimeMillisDelay( task, getTimeMillisToAfterDaysHour( delayDays, hourOfTomorrow ), period );
	}
	
	public static long getTimeMillisToAfterDaysHour( int days, int hourOfTomorrow ) throws Exception {

		if ( 0 == hourOfTomorrow )
			hourOfTomorrow = 2;

		Calendar calendar = Calendar.getInstance();

		int yearOfToday = calendar.get( Calendar.YEAR );
		int monthOfToday = calendar.get( Calendar.MONTH ) + 1;
		int dayOfToday = calendar.get( Calendar.DAY_OF_MONTH );

		calendar.set( Calendar.DAY_OF_MONTH, dayOfToday + days );
		if ( 31 == dayOfToday && days >=1 ) {
			calendar.set( Calendar.MONTH, monthOfToday + 1 );
		}
		if ( 12 == monthOfToday && 31 == dayOfToday && days>=1 ) {
			calendar.set( Calendar.YEAR, yearOfToday + 1 );
		}

		int dayOfTomorrow  = calendar.get( Calendar.DAY_OF_MONTH );
		int monthOfTomorrow  = calendar.get( Calendar.MONTH );
		int yearOfTomorrow = calendar.get( Calendar.YEAR );

		Calendar calendarOfTomorrow = new GregorianCalendar( yearOfTomorrow, monthOfTomorrow, dayOfTomorrow, hourOfTomorrow, 0, 0 );
		long startTimeMillis = System.currentTimeMillis();
		
		
		long timeMillisToAfterDaysHour = calendarOfTomorrow.getTime().getTime() - startTimeMillis; 
		
		if( 0 > timeMillisToAfterDaysHour )
			throw new Exception( "时间差为负数，可能设置有误" );
		
		return timeMillisToAfterDaysHour;

	}
	
	/**
	 * 
	 * Description:线程睡眠
	 * @param sleepTime
	 */
	public static void sleep(long sleepTime){
		
		try {
			Thread.sleep(sleepTime);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
