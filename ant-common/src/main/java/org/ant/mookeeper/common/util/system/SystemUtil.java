package org.ant.mookeeper.common.util.system;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.InetAddress;
import java.util.Properties;

import org.ant.mookeeper.common.constant.BaseConstant;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.SystemUtils;

public class SystemUtil extends SystemUtils{

	/**
	 * 加载配置文件
	 * @return Properties
	 * @throws IOException
	 */
	public static Properties loadProperty() throws IOException{
			
			String filePathOfProperty = StringUtils.trimToEmpty( System.getProperty( BaseConstant.SYSTEM_PROPERTY_CONFIG_FILE_PATH ) );
			if( StringUtils.isBlank( filePathOfProperty ) ){
				throw new FileNotFoundException( "Please defined,such as -DconfigFilePath=\"W:\\TaoKeeper\\taokeeper\\config\\config-test.properties\"" );
			}
			Properties properties = new Properties();
			properties.load(IOUtils.toInputStream(FileUtils.readFileToString(new File(filePathOfProperty), "UTF-8")));
			
			return properties;
	}
	
	/**
	 * 获取本机IP地址
	 * @return 本机IP
	 */
	public static String getIPAddress() {
		InetAddress inetAddress = null;
		try {
			inetAddress = InetAddress.getLocalHost();
			if ( null == inetAddress ) {
				return "Unknow ip";
			} else {
				return inetAddress.getHostAddress();
			}
		} catch ( Exception e ) {
			return "Unknow ip";
		}
	}
}
