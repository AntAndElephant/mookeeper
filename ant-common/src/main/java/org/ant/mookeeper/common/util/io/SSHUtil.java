package org.ant.mookeeper.common.util.io;

import static org.ant.mookeeper.common.constant.BaseConstant.WORD_SEPARATOR;
import static org.ant.mookeeper.common.constant.EmptyObjectConstant.EMPTY_STRING;
import static org.ant.mookeeper.common.constant.HtmlTagConstant.BR;
import static org.ant.mookeeper.common.constant.SymbolConstant.COMMA;
import static org.ant.mookeeper.common.constant.SymbolConstant.PERCENT;
import static org.ant.mookeeper.common.constant.SymbolConstant.POINT;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

import org.ant.mookeeper.common.entity.HostPerformanceEntity;
import org.ant.mookeeper.common.entity.io.SSHResource;
import org.ant.mookeeper.common.exception.IllegalParamException;
import org.ant.mookeeper.common.exception.SSHException;
import org.apache.commons.lang3.StringUtils;

import ch.ethz.ssh2.Connection;
import ch.ethz.ssh2.Session;
import ch.ethz.ssh2.StreamGobbler;

public class SSHUtil {

	private final static String USERNAME = "nobody";
	private final static String PASSWORD = "look";

	private final static String COMMAND_TOP = "top -b -n 1 | head -5";
	private final static String COMMAND_DF_LH = "df -lh";
	private final static String LOAD_AVERAGE_STRING = "load average: ";
	private final static String CPU_USAGE_STRING = "%Cpu(s):";
	private final static String MEM_USAGE_STRING = "KiB Mem :";
	private final static String SWAP_USAGE_STRING = "KiB Swap:";

	/**
	 * Get HostPerformanceEntity[cpuUsage, memUsage, load] by ssh.<br>
	 * 方法返回前已经释放了所有资源，调用方不需要关心
	 * 
	 * @param ip
	 * @param userName
	 * @param password
	 * @throws Exception
	 * @since 1.0.0
	 */
	public static HostPerformanceEntity getHostPerformance( String ip, int port, String userName, String password ) throws SSHException {

		if ( StringUtils.isBlank(ip) ) {
			try {
				throw new IllegalParamException( "Param ip is empty!" );
			} catch ( IllegalParamException e ) {
				throw new SSHException( e.getMessage(), e );
			}
		}
		port = port < 0? 22 : port;
		userName = StringUtils.defaultIfBlank(userName, USERNAME);
		password = StringUtils.defaultIfBlank(password, PASSWORD);
		Connection conn = null;
		try {
			conn = new Connection( ip, port );
			conn.connect();
			boolean isAuthenticated = conn.authenticateWithPassword( userName, password );
			if ( isAuthenticated == false ) {
				throw new Exception( "SSH authentication failed with [ userName: " + userName + ", password: " + password + "]" );
			}
			return getHostPerformance( conn );
		} catch ( Exception e ) {
			throw new SSHException( "SSH 连接错误", e );
		} finally {
			if ( null != conn )
				conn.close();
		}
	}

	/**
	 * GetSystemPerformance
	 * 
	 * @param conn a connection
	 * @return double cpu usage
	 * @throws Exception
	 */
	@SuppressWarnings("resource")
	private static HostPerformanceEntity getHostPerformance( Connection conn ) throws Exception {

		HostPerformanceEntity systemPerformanceEntity = null;
		Session session = null;
		BufferedReader read = null;
		try {
			systemPerformanceEntity = new HostPerformanceEntity();
			systemPerformanceEntity.setIp( conn.getHostname() );
			session = conn.openSession();
			session.execCommand( COMMAND_TOP );

			read = new BufferedReader( new InputStreamReader( new StreamGobbler( session.getStdout() ) ) );
			String line = "";
			int lineNum = 0;

			String totalMem = EMPTY_STRING;
			String freeMem = EMPTY_STRING;
			String buffersMem = EMPTY_STRING;
			String cachedMem = EMPTY_STRING;
			while ( ( line = read.readLine() ) != null ) {

				if ( StringUtils.isBlank( line ) )
					continue;
				lineNum += 1;

				if ( 5 < lineNum )
					return systemPerformanceEntity;

				if ( 1 == lineNum ) {
					// 第一行，通常是这样：(centos7)
					//top - 15:53:52 up  3:36,  2 users,  load average: 0.01, 0.02, 0.05
					int loadAverageIndex = line.indexOf( LOAD_AVERAGE_STRING );
					String loadAverages = line.substring( loadAverageIndex ).replace( LOAD_AVERAGE_STRING, EMPTY_STRING );
					String[] loadAverageArray = loadAverages.split( "," );
					if ( 3 != loadAverageArray.length )
						continue;
					systemPerformanceEntity.setLoad( StringUtils.trimToEmpty( loadAverageArray[0] ) );
				} else if ( 3 == lineNum ) {
					// 第三行通常是这样：(centos7 系统)
					//%Cpu(s):  0.3 us,  0.6 sy,  0.0 ni, 99.1 id,  0.0 wa,  0.0 hi,  0.0 si,  0.0 st
					String cpuUsage = line.split( "," )[0].replace( CPU_USAGE_STRING, EMPTY_STRING ).replace( "us", EMPTY_STRING );
					systemPerformanceEntity.setCpuUsage( StringUtils.trimToEmpty( cpuUsage ) );
				} else if ( 4 == lineNum ) {
					// 第四行通常是这样：(centos7 系统)
					//KiB Mem :  3865552 total,  3222792 free,   302252 used,   340508 buff/cache
					String[] memArray = line.replace( MEM_USAGE_STRING, EMPTY_STRING ).split( COMMA );
					totalMem = StringUtils.trimToEmpty( memArray[0].replace( "total", EMPTY_STRING ) );
					freeMem = StringUtils.trimToEmpty( memArray[1].replace( "free", EMPTY_STRING ) );
					buffersMem = StringUtils.trimToEmpty( memArray[3].replace( "buff/cache", EMPTY_STRING ) );
				} else if ( 5 == lineNum ) {
					// 第五行通常是这样：
					//KiB Swap:  2097148 total,  2097148 free,        0 used.  3265368 avail Mem 
					String[] memArray = line.replace( SWAP_USAGE_STRING, EMPTY_STRING ).split( COMMA );
					cachedMem = StringUtils.trimToEmpty( memArray[2].split(POINT)[0].replace("used", EMPTY_STRING) );

					if ( StringUtils.isAnyBlank(totalMem, freeMem, buffersMem, cachedMem ) )
						throw new Exception( "Error when get system performance of ip: " + conn.getHostname()
								+ ", can't get totalMem, freeMem, buffersMem or cachedMem" );

					Double totalMemDouble = Double.parseDouble( totalMem );
					Double freeMemDouble = Double.parseDouble( freeMem );
					Double buffersMemDouble = Double.parseDouble( buffersMem );
					Double cachedMemDouble = Double.parseDouble( cachedMem );

					Double memoryUsage = 1 - ( ( freeMemDouble + buffersMemDouble + cachedMemDouble ) / totalMemDouble );
					systemPerformanceEntity.setMemoryUsage( memoryUsage * 100 + PERCENT );
				} else {
					continue;
				}
			}// parse the top output

			// 统计磁盘使用状况
			Map< String, String > diskUsageMap = new HashMap< String, String >();
			session = conn.openSession();
			session.execCommand( COMMAND_DF_LH );
			read = new BufferedReader( new InputStreamReader( new StreamGobbler( session.getStdout() ) ) );

			/**
			 * Filesystem      Size  Used Avail Use% Mounted on
			 *	/dev/vda1        40G  2.7G   35G   8% /
			 *	devtmpfs        1.9G     0  1.9G   0% /dev
			 *	tmpfs           1.9G     0  1.9G   0% /dev/shm
			 *	tmpfs           1.9G  368K  1.9G   1% /run
			 *	tmpfs           1.9G     0  1.9G   0% /sys/fs/cgroup
			 *	tmpfs           380M     0  380M   0% /run/user/0
			 */
			
			boolean isFirstLine = true;
			while ( ( line = read.readLine() ) != null ) {

				if ( isFirstLine ) {
					isFirstLine = false;
					continue;
				}
				if ( StringUtils.isBlank( line ) )
					continue;

				line = line.replaceAll( " {1,}", WORD_SEPARATOR );
				String[] lineArray = line.split( WORD_SEPARATOR );
				if ( 6 != lineArray.length ) {
					continue;
				}
				String diskUsage = lineArray[4];
				String mountedOn = lineArray[5];
				diskUsageMap.put( mountedOn, diskUsage );
			}
			systemPerformanceEntity.setDiskUsageMap( diskUsageMap );

		} catch ( Exception e ) {
			throw new Exception( "Error when get system performance of ip: " + conn.getHostname(), e );
		} finally {
			try {
				if ( null != read )
					read.close();
				if ( null != session )
					session.close();
			} catch ( Exception e ) {
				// ingore
			}
		}
		return systemPerformanceEntity;
	}

	/**
	 * SSH 方式登录远程主机，执行命令,方法内部会关闭所有资源，调用方无须关心。
	 * @param ip 主机ip
	 * @param username 用户名
	 * @param password 密码
	 * @param command 要执行的命令
	 */
	public static String execute( String ip, int port, String username, String password, String command ) throws SSHException {

		if ( StringUtils.isBlank( command ) )
			return EMPTY_STRING;
		port = port < 0? 22 : port;
		Connection conn = null;
		Session session = null;
		BufferedReader read = null;
		StringBuffer sb = new StringBuffer();
		try {
			if ( StringUtils.isBlank( ip ) ) {
				throw new IllegalParamException( "Param ip is empty!" );
			}
			username = StringUtils.defaultIfBlank( username, USERNAME );
			password = StringUtils.defaultIfBlank( password, PASSWORD );
			conn = new Connection( ip, port );
			conn.connect( null, 2000, 2000 );
			boolean isAuthenticated = conn.authenticateWithPassword( username, password );
			if ( isAuthenticated == false ) {
				throw new Exception( "SSH authentication failed with [ userName: " + username + ", password: " + password + "]" );
			}

			session = conn.openSession();
			session.execCommand( command );

			read = new BufferedReader( new InputStreamReader( new StreamGobbler( session.getStdout() ) ) );
			String line = "";
			while ( ( line = read.readLine() ) != null ) {
				sb.append( line ).append( BR );
			}
			return sb.toString();
		} catch ( Exception e ) {
			throw new SSHException( "SSH远程执行command: " + command + " 出现错误: " + e.getMessage(), e );
		} finally {
			if ( null != read ) {
				try {
					read.close();
				} catch ( IOException e ) {
				}
			}
			if ( null != session )
				session.close();
			if ( null != conn )
				conn.close();
		}
	}
	
	
	
	
	
	

	/**
	 * SSH 方式登录远程主机，执行命令,方法内部会关闭所有资源,此方法不会返回任何内容。
	 * @param ip 主机ip
	 * @param username 用户名
	 * @param password 密码
	 * @param command 要执行的命令
	 */
	public static void executeNoOutPut( String ip, int port, String username, String password, String command ) throws SSHException {

		if ( StringUtils.isBlank( command ) )
			return;
		port = port < 0? 22 : port;
		Connection conn = null;
		Session session = null;
		BufferedReader read = null;
		try {
			if ( StringUtils.isBlank( ip ) ) {
				throw new IllegalParamException( "Param ip is empty!" );
			}
			username = StringUtils.defaultIfBlank( username, USERNAME );
			password = StringUtils.defaultIfBlank( password, PASSWORD );
			conn = new Connection( ip, port );
			conn.connect( null, 2000, 2000 );
			boolean isAuthenticated = conn.authenticateWithPassword( username, password );
			if ( isAuthenticated == false ) {
				throw new Exception( "SSH authentication failed with [ userName: " + username + ", password: " + password + "]" );
			}

			session = conn.openSession();
			session.execCommand( command );
			Thread.sleep( 10000 );

		} catch ( Exception e ) {
			throw new SSHException( "SSH远程执行command: " + command + " 出现错误: " + e.getMessage(), e );
		} finally {
			if ( null != read ) {
				try {
					read.close();
				} catch ( IOException e ) {
				}
			}
			if ( null != session )
				session.close();
			if ( null != conn )
				conn.close();
		}
	}

	/**
	 * SSH
	 * 方式登录远程主机，执行命令,方法内部没有关闭资源，调用方需要调用SSHResource.closeAllResource()来关闭所有资源。
	 * @param ip 主机ip
	 * @param username 用户名
	 * @param password 密码
	 * @param command 要执行的命令
	 * @return SSHResource 包含Connection, Session, BufferedReader
	 */
	public static SSHResource executeWithoutHandleBufferedReader( String ip, int port, String username, String password, String command ) throws SSHException {

		if ( StringUtils.isBlank( command ) )
			return null;
		port = port < 0? 22 : port;
		SSHResource sshResource = new SSHResource();

		try {
			if ( StringUtils.isBlank( ip ) ) {
				throw new IllegalParamException( "Param ip is empty!" );
			}
			username = StringUtils.defaultIfBlank( username, USERNAME );
			password = StringUtils.defaultIfBlank( password, PASSWORD );
			sshResource.conn = new Connection( ip, port );
			sshResource.conn.connect( null, 2000, 2000 );
			boolean isAuthenticated = sshResource.conn.authenticateWithPassword( username, password );
			if ( isAuthenticated == false ) {
				throw new Exception( "SSH authentication failed with [ userName: " + username + ", password: " + password + "]" );
			}

			sshResource.session = sshResource.conn.openSession();
			sshResource.session.execCommand( command );
			sshResource.setReader( new BufferedReader( new InputStreamReader( new StreamGobbler( sshResource.session.getStdout() ) ) ) );

			return sshResource;
		} catch ( Exception e ) {
			throw new SSHException( "SSH远程执行command: " + command + " 出现错误: " + e.getMessage(), e );
		}
	}
	
}
