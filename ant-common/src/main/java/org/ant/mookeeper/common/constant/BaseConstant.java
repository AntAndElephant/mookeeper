package org.ant.mookeeper.common.constant;

public class BaseConstant {
	/** mill seconds of one day */
	public static long MILLISECONDS_OF_ONE_MINUTE = 1000 * 60;
	
	/** mill seconds of one day */
	public static long MILLISECONDS_OF_ONE_DAY = 1000 * 60 * 60 * 24;
	
	/**
	 * WORD_SEPARATOR  ( char )2
	 */
	public static final String WORD_SEPARATOR = Character.toString( ( char )2 );
	
	public static final String SYSTEM_PROPERTY_CONFIG_FILE_PATH = "configFilePath";
}
