package org.ant.mookeeper.web.monitor.core;

import java.util.Properties;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

import org.ant.mookeeper.common.SystemInfo;
import org.ant.mookeeper.common.constant.SystemConstant;
import org.ant.mookeeper.common.util.ThreadUtil;
import org.ant.mookeeper.common.util.system.SystemUtil;
import org.ant.mookeeper.web.monitor.task.HostPerformanceCollectTask;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class Initialization extends HttpServlet{

	static Logger LOG = LogManager.getLogger(Initialization.class);
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7426668072381491284L;
	
	@Override
	public void init() throws ServletException {
		// TODO Auto-generated method stub
		super.init();
		//初始化线程池
		ThreadPoolManager.init();
		
		initSystem();
		
		//收集机器的信息
		ThreadUtil.startThread( new HostPerformanceCollectTask() );
	}
	
	private void initSystem(){
		LOG.info("=================================Start to init system===========================");
		Properties properties = null;
		
		try {
			properties = SystemUtil.loadProperty();
			if(properties == null){
				throw new Exception( "Please defined,such as -DconfigFilePath=\"W:\\TaoKeeper\\taokeeper\\config\\config-test.properties\"" );
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			LOG.error(e.getMessage(), e);
			throw new RuntimeException( e.getMessage(), e.getCause() );
		}
		
		SystemInfo.envName = StringUtils.defaultIfBlank( properties.getProperty( "systemInfo.envName" ), "mookeeper" );
		SystemConstant.dataStoreBasePath = StringUtils.defaultIfBlank( properties.getProperty( "SystemConstent.dataStoreBasePath" ),
				"/home/yinshi.nc/taokeeper-monitor/" );
		SystemConstant.userNameOfSSH = StringUtils.defaultIfBlank( properties.getProperty( "SystemConstant.userNameOfSSH" ), "root" );
		SystemConstant.passwordOfSSH = StringUtils.defaultIfBlank( properties.getProperty( "SystemConstant.passwordOfSSH" ), "123456" );
		SystemConstant.portOfSSH = NumberUtils.toInt(properties.getProperty( "SystemConstant.portOfSSH" ), 22);

		SystemConstant.IP_OF_MESSAGE_SEND = StringUtils.trimToEmpty( properties.getProperty( "SystemConstant.IP_OF_MESSAGE_SEND" ) );
		LOG.info( "=================================Finish init system===========================" );
	
		
	}
}
