package org.ant.mookeeper.web.monitor.task;

import static org.ant.mookeeper.common.constant.SymbolConstant.COLON;
import static org.ant.mookeeper.common.constant.SystemConstant.MINS_RATE_OF_COLLECT_HOST_PERFORMANCE;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import static org.ant.mookeeper.common.constant.SystemConstant.MINS_RATE_OF_COLLECT_HOST_PERFORMANCE;
import org.ant.mookeeper.common.GlobalInstance;
import org.ant.mookeeper.common.model.AlarmSettings;
import org.ant.mookeeper.common.model.ZooKeeperCluster;
import org.ant.mookeeper.common.util.ThreadUtil;
import org.ant.mookeeper.web.monitor.core.ThreadPoolManager;
import org.ant.mookeeper.web.monitor.task.runable.ZKServerPerformanceCollector;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


/**
 * 
 * Description:获取zookeeper服务器的状态
 * @author mxsm
 * @Date 2017-06-18
 */

public class HostPerformanceCollectTask implements Runnable{

	static Logger LOG = LogManager.getLogger(HostPerformanceCollectTask.class);
	
	@SuppressWarnings("unused")
	@Override
	public void run() {
		// TODO Auto-generated method stub
		while (true) {
			//type type = (type) en.nextElement();
			if(!GlobalInstance.need_host_performance_collect){
				LOG.info("No need to host_performance_collect, need_host_performance_collect=" + GlobalInstance.need_host_performance_collect);
				ThreadUtil.sleep( 1000 * 60 * MINS_RATE_OF_COLLECT_HOST_PERFORMANCE  );
				continue;
			}
			
			ThreadPoolManager.addJobToZKServerPerformanceCollectorExecutor(new ZKServerPerformanceCollector("192.168.31.65", null, null));
			
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

}
