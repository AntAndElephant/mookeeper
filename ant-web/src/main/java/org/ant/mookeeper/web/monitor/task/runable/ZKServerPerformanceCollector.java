package org.ant.mookeeper.web.monitor.task.runable;

import java.util.Map;

import org.ant.mookeeper.common.GlobalInstance;
import org.ant.mookeeper.common.constant.EmptyObjectConstant;
import org.ant.mookeeper.common.constant.SymbolConstant;
import org.ant.mookeeper.common.constant.SystemConstant;
import org.ant.mookeeper.common.entity.HostPerformanceEntity;
import org.ant.mookeeper.common.exception.SSHException;
import org.ant.mookeeper.common.model.AlarmSettings;
import org.ant.mookeeper.common.model.ZooKeeperCluster;
import org.ant.mookeeper.common.util.io.SSHUtil;
import org.ant.mookeeper.web.monitor.core.Initialization;
import org.ant.mookeeper.web.monitor.core.ThreadPoolManager;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ZKServerPerformanceCollector implements Runnable{

	static Logger LOG = LogManager.getLogger(ZKServerPerformanceCollector.class);
	
	private String ip;
	private AlarmSettings alarmSettings;
	private ZooKeeperCluster zookeeperCluster;

	/**
	 * @param ip  
	 * @param alarmSettings  
	 * @param zookeeperCluster 
	 */
	public ZKServerPerformanceCollector( String ip, AlarmSettings alarmSettings, ZooKeeperCluster zookeeperCluster ) {
		this.ip = ip;
		this.alarmSettings = alarmSettings;
		this.zookeeperCluster = zookeeperCluster;
	}

	@Override
	public void run() {
		try {
			HostPerformanceEntity hostPerformanceEntity = SSHUtil.getHostPerformance( ip, SystemConstant.portOfSSH, SystemConstant.userNameOfSSH, SystemConstant.passwordOfSSH );
			//sendAlarm( alarmSettings, hostPerformanceEntity, zookeeperCluster.getClusterName() );
			
			//GlobalInstance.putHostPerformanceEntity( ip, hostPerformanceEntity );
			LOG.info( hostPerformanceEntity );
		} catch ( SSHException e ) {
			LOG.warn( "HostPerformanceEntity collect of " + ip + " ：" + e.getMessage() );
		} catch ( Throwable exception ) {
			LOG.error( "程序出错: " + exception.getMessage() );
			exception.printStackTrace();
		}
	}

	/**
	 * check if alarm
	 */
	public void sendAlarm( AlarmSettings alarmSettings, HostPerformanceEntity hostPerformanceEntity, String clusterName ) {

		if ( null == alarmSettings )
			return;

		String wangwangList = alarmSettings.getWangwangList();
		String phoneList = alarmSettings.getPhoneList();

		String maxCpuUsage = alarmSettings.getMaxCpuUsage();
		String maxMemoryUsage = alarmSettings.getMaxMemoryUsage();
		String maxLoad = alarmSettings.getMaxLoad();

		String dataDir = alarmSettings.getDataDir();
		String dataLogDir = alarmSettings.getDataLogDir();
		String maxDiskUsage = alarmSettings.getMaxDiskUsage();

		if ( !StringUtils.isBlank( maxCpuUsage ) ) { // Cpu usage alarm
			String cpuUsage = hostPerformanceEntity.getCpuUsage();
			if ( !StringUtils.isBlank( cpuUsage ) && cpuUsage.endsWith( "%" ) ) {
				cpuUsage = cpuUsage.replaceAll( "%", "" );
				double difference = Double.parseDouble( cpuUsage ) - Double.parseDouble( maxCpuUsage );
				if ( 0 < difference ) {
					LOG.warn( "ZK Server " + hostPerformanceEntity.getIp() + " cpu Usage too high：" + cpuUsage + "-" + maxCpuUsage + "=" + difference );
					// Alarm
					if ( GlobalInstance.needAlarm.get() ) {
						LOG.info( "WangWangList: " + wangwangList );
					}
				}
			}
		}

		if ( !StringUtils.isBlank( maxMemoryUsage ) ) { // Memory usage alarm
			String memoryUsage = hostPerformanceEntity.getMemoryUsage();
			if ( !StringUtils.isBlank( memoryUsage ) && memoryUsage.endsWith( "%" ) ) {
				memoryUsage = memoryUsage.replaceAll( "%", "" );
				double difference = Double.parseDouble( memoryUsage ) - Double.parseDouble( maxMemoryUsage );
				if ( 0 < difference ) {
					LOG.warn( "ZK Server "+ hostPerformanceEntity.getIp() +" memory usage too high: " + memoryUsage + "-" + maxMemoryUsage + "=" + difference );
					// Alarm
					if ( GlobalInstance.needAlarm.get() ) {
						
						LOG.info( "WangWangList: " + wangwangList );
					}
				}
			}
		}

		if ( !StringUtils.isBlank( maxLoad ) ) { // Load usage alarm
			String load = hostPerformanceEntity.getLoad();
			if ( !StringUtils.isBlank( load ) ) {
				double difference = Double.parseDouble( load ) - Double.parseDouble( maxLoad );
				if ( 0 < difference ) {
					LOG.warn( "ZK Server "+ hostPerformanceEntity.getIp() +" load usage too high: " + load + "-" + maxLoad + "=" + difference );
					if ( GlobalInstance.needAlarm.get() ) {
						
						
						LOG.info( "WangWangList: " + wangwangList );
					}
				}
			}
		}

		try {
			if ( !StringUtils.isBlank( dataDir ) || !StringUtils.isBlank( dataLogDir ) ) { // 需要进行
																							// disk容量
																							// 报警

				dataDir = StringUtils.trimToEmpty( dataDir );
				dataLogDir = StringUtils.trimToEmpty( dataLogDir );

				if ( !StringUtils.isBlank( maxDiskUsage ) ) {

					Map< String, String > diskUsageMap = hostPerformanceEntity.getDiskUsageMap();
					if ( null != diskUsageMap ) {
						for ( String mountedOn : diskUsageMap.keySet() ) {

							if ( StringUtils.trimToEmpty( mountedOn ).equalsIgnoreCase( SymbolConstant.SLASH ) )
								continue;

							if ( dataDir.startsWith( StringUtils.trimToEmpty( mountedOn ) )
									|| dataLogDir.startsWith( StringUtils.trimToEmpty( mountedOn ) ) ) {
								int diskUsage = Integer.parseInt( StringUtils.trimToEmpty( diskUsageMap.get( mountedOn ) ).replace( SymbolConstant.PERCENT,
										EmptyObjectConstant.EMPTY_STRING ) );
								if ( diskUsage > Integer.parseInt( maxDiskUsage ) ) {
									LOG.warn( "ZK Server " + hostPerformanceEntity.getIp() + " disk usage too high, " + mountedOn + ": " + diskUsage
											+ "%, max setting usage is: " + maxDiskUsage + "%" );
									if ( GlobalInstance.needAlarm.get() ) {
										
										LOG.info( "WangWangList: " + wangwangList );
									}
								}
							}
						}
					}
				}

			}
		} catch ( Throwable e ) {
			LOG.error( "Error when ckeck disk usage：" + e.getMessage() );
			e.printStackTrace();
		}// disk alarm

	}

}
