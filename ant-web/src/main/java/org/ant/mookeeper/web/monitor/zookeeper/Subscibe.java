package org.ant.mookeeper.web.monitor.zookeeper;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.zookeeper.KeeperException;
import org.apache.zookeeper.WatchedEvent;
import org.apache.zookeeper.Watcher;
import org.apache.zookeeper.Watcher.Event.EventType;
import org.apache.zookeeper.Watcher.Event.KeeperState;
import org.apache.zookeeper.ZooKeeper;
import org.apache.zookeeper.data.Stat;

public class Subscibe implements Watcher{

	static Logger LOG = LogManager.getLogger(Subscibe.class);
	
	private ZooKeeper zk;
	
	public void init(){
		
		try {
			zk = new ZooKeeper("localhost:2181", 5000, this);
			
			//LOG.info(new String(zk.getData("/test1/aaaa", true, new Stat())));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Override
	public void process(WatchedEvent event) {
		// zkclient connected
		LOG.info(event.getType());
		if(event.getState() == KeeperState.SyncConnected){
			if(event.getType() == EventType.NodeDataChanged){
				try {
					LOG.info(new String(zk.getData("/test1/aaaa", false, new Stat())));
				} catch (KeeperException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}

}
