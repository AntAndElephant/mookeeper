package org.ant.mookeeper.boot.monitor.db;

import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.alibaba.druid.pool.DruidDataSource;
import com.alibaba.druid.pool.DruidDataSourceFactory;

/**
 * 
 * Description:DB 工具类
 * @author mxsm
 * @Date 2017-06-18
 */

public class DBUtil {

	static Logger LOG = LoggerFactory.getLogger(DBUtil.class);
	
    public static String url;
    
    public static String username;
    
    public static String password;
    
    public static String filters="stat";
    
    public static String maxActive="20";
    
    public static String initialSize="1";
    
    public static String maxWait="60000";
    
    public static String minIdle="1";
    
    public static String timeBetweenEvictionRunsMillis="60000";
    
    public static String minEvictableIdleTimeMillis="300000";
    
    public static String testWhileIdle="true";
    
    public static String testOnBorrow="false";
    
    public static String testOnReturn="false";
    
    public static String poolPreparedStatements="true";
    
    public static String maxOpenPreparedStatements="20";
	
    public static DruidDataSource druidDataSource;
    
    public static Properties properties;
    
    /**
     * 
     * Description:初始化数据库
     */
    public static void init() throws Exception{
    	
    	if(properties == null){
    		properties = new Properties();
    		
    		properties.setProperty("url", url);
    		properties.setProperty("username", username);
    		properties.setProperty("password", password);
    		properties.setProperty("filters", filters);
    		properties.setProperty("maxActive", maxActive);
    		properties.setProperty("initialSize", initialSize);
    		properties.setProperty("maxWait", maxWait);
    		properties.setProperty("minIdle", minIdle);
    		properties.setProperty("timeBetweenEvictionRunsMillis", timeBetweenEvictionRunsMillis);
    		properties.setProperty("minEvictableIdleTimeMillis", minEvictableIdleTimeMillis);
    		properties.setProperty("testWhileIdle", testWhileIdle);
    		properties.setProperty("testOnBorrow", testOnBorrow);
    		properties.setProperty("testOnReturn", testOnReturn);
    		properties.setProperty("poolPreparedStatements", poolPreparedStatements);
    		properties.setProperty("maxOpenPreparedStatements", maxOpenPreparedStatements);
    	}
    	
    	try {
    		druidDataSource = (DruidDataSource)DruidDataSourceFactory.createDataSource(properties);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			throw new Exception( "create DruidDataSource fail: " + e.getMessage(), e.getCause() );
		}
    }
    
   
}
