package org.ant.mookeeper.boot.config;

import org.ant.mookeeper.boot.monitor.core.Initialization;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author mxsm
 * @Date 2017-07-09
 * Description:系统配置文件
 */

@Configuration
public class ApplictionConfig {

	@Bean(initMethod="init",name="initialization")
	public Initialization initialization() {
		return new Initialization();
	}
	
}
