package org.ant.mookeeper.boot.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * 
 * @author mxsm
 * @Date 2017-07-09
 * Description
 */
@Component
@Scope("singleton")
public class SpringBeanUtil implements ApplicationContextAware{
	
	static Logger LOG = LoggerFactory.getLogger(SpringBeanUtil.class);
	
	private static ApplicationContext applicationContext;
	
	private Object lock = new Object();
	
	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		// TODO Auto-generated method stub
		synchronized (lock) {
			if(null == SpringBeanUtil.applicationContext) {
				LOG.info("=========  SpringBeanUtil Init success  ===========");
				SpringBeanUtil.applicationContext = applicationContext;
			}
		}
	}
	
	     // 获取applicationContext
	     public static ApplicationContext getApplicationContext() {
	         return applicationContext;
	     }
	
	     // 通过name获取 Bean.
	     public static Object getBean(String name) {
	         return getApplicationContext().getBean(name);
	     }
	 
	     // 通过class获取Bean.
	     public static <T> T getBean(Class<T> clazz) {
	         return getApplicationContext().getBean(clazz);
	     }
	 
	     // 通过name,以及Clazz返回指定的Bean
	     public static <T> T getBean(String name, Class<T> clazz) {
	         return getApplicationContext().getBean(name, clazz);
	     }
	
}
