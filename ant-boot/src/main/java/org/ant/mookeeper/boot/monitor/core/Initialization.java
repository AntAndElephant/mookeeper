package org.ant.mookeeper.boot.monitor.core;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author mxsm
 * @Date 2017-07-09
 * Description 所有的启动项
 */

public class Initialization {

	static Logger LOG = LoggerFactory.getLogger(Initialization.class);
	
	
	
	public void init() {
		LOG.info("================= initializing start =====================");
		
		
		
		
		LOG.info("================= initializing end [success] =====================");
	}
	
}
