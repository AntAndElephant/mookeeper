package org.ant.mookeeper.boot.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.core.env.Environment;

@Configuration
@PropertySources({@PropertySource("file:${configFileLocation}mookeeper.properties")})
public class ProprertiesConfig {

	@Autowired
    Environment env;
	
}
